package com.herokuapp.cargueroscolombia.models.getmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Conductor{

	private String correo;
	private String celular;
	private long id;
	private String nombre;
	private Integer edad;

	public void setCorreo(String correo){
		this.correo = correo;
	}

	@JsonProperty("correo")
	public String getCorreo(){
		return correo;
	}

	public void setCelular(String celular){
		this.celular = celular;
	}

	@JsonProperty("celular")
	public String getCelular(){
		return celular;
	}

	public void setId(long id){
		this.id = id;
	}

	@JsonProperty("id")
	public long getId(){
		return id;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	@JsonProperty("nombre")
	public String getNombre(){
		return nombre;
	}

	public void setEdad(Integer edad){
		this.edad = edad;
	}

	@JsonProperty("edad")
	public Integer getEdad(){
		return edad;
	}
}
