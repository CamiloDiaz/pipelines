package com.herokuapp.cargueroscolombia.models.getmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.herokuapp.cargueroscolombia.util.JsonConverter;

public class Vehiculo extends JsonConverter {
	private String marca;
	private String tipo;
	private Conductor conductor;
	private Integer anio;
	private String placa;
	private Integer capacidad;
	private Boolean tipoValid;

	public void setMarca(String marca){
		this.marca = marca;
	}

	@JsonProperty("marca")
	public String getMarca(){
		return marca;
	}

	public void setTipo(String tipo){
		this.tipo = tipo;
	}

	@JsonProperty("tipo")
	public String getTipo(){
		return tipo;
	}

	public void setConductor(Conductor conductor){
		this.conductor = conductor;
	}
	@JsonProperty("conductor")
	public Conductor getConductor(){
		return conductor;
	}

	public void setAnio(Integer anio){
		this.anio = anio;
	}

	@JsonProperty("anio")
	public Integer getAnio(){
		return anio;
	}

	public void setPlaca(String placa){
		this.placa = placa;
	}

	@JsonProperty("placa")
	public String getPlaca(){
		return placa;
	}

	public void setCapacidad(Integer capacidad){
		this.capacidad = capacidad;
	}

	@JsonProperty("capacidad")
	public Integer getCapacidad(){
		return capacidad;
	}

	@JsonProperty("tipoValid")
	public Boolean getTipoValid() {
		return tipoValid;
	}

	public void setTipoValid(Boolean tipoValid) {
		this.tipoValid = tipoValid;
	}

}
