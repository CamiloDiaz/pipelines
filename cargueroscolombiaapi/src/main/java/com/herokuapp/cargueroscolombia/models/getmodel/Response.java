package com.herokuapp.cargueroscolombia.models.getmodel;

import java.util.List;

public class Response{
	private List<Vehiculo> response;

	public void setResponse(List<Vehiculo> response){
		this.response = response;
	}

	public List<Vehiculo> getResponse(){
		return response;
	}
}