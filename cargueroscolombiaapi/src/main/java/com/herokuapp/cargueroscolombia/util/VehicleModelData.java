package com.herokuapp.cargueroscolombia.util;

import com.github.javafaker.Faker;
import com.herokuapp.cargueroscolombia.models.getmodel.Conductor;
import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;

import static com.herokuapp.cargueroscolombia.util.Dictionary.SPACE_STRING;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.*;

public class VehicleModelData {

    private VehicleModelData() {
        throw new AssertionError("Instantiating utility class.");
    }

    public static Vehiculo vehicleModelInformation(){

        Faker faker = new Faker();

        int anio = faker.number().numberBetween(TWO_THOUSAND.getValue(), TWO_THOUSAND_AND_TWENTY_THREE.getValue());
        int capacidad= faker.number().numberBetween(ONE.getValue(), MAX_CAPACITY.getValue());
        String celular= String.valueOf(faker.number().numberBetween(MIN_NUM.getValue(), MAX_NUM.getValue()));
        String primerNombre = faker.name().firstName();
        String apellido = faker.name().lastName();
        String nombre = primerNombre.concat(SPACE_STRING).concat(apellido);
        String correo = faker.internet().emailAddress();
        int edad = faker.number().numberBetween(EIGHTEEN.getValue(), NINETY_NINE.getValue());
        int numId = faker.number().numberBetween(MIN_ID_NUM.getValue(), MAX_ID_NUM.getValue());
        String addId = String.valueOf(numId) + FOUR.getValue();
        long id = Long.parseLong(addId);

        String numerosPlaca = String.valueOf(faker.number().numberBetween(ONE_HUNDRED.getValue(), NINE_HOUNDRED_NINETY_NINE.getValue()));
        int tipoVehiculo = faker.number().numberBetween(ZERO.getValue(), THREE.getValue());
        int marcaVehiculo = faker.number().numberBetween(ZERO.getValue(), SIX.getValue());
        String tipo = escogerTipo(tipoVehiculo);
        String marca = escogerMarca(marcaVehiculo);
        String letras = faker.letterify("???").toUpperCase();
        String placa = letras.concat(numerosPlaca);

        Conductor conductor = new Conductor();
        conductor.setCelular(celular+FIVE.getValue());
        conductor.setCorreo(correo);
        conductor.setEdad(edad);
        conductor.setId(id);
        conductor.setNombre(nombre);

        Vehiculo vehicleModel = new Vehiculo();

        vehicleModel.setAnio(anio);
        vehicleModel.setCapacidad(capacidad);
        vehicleModel.setConductor(conductor);
        vehicleModel.setMarca(marca);
        vehicleModel.setPlaca(placa);
        vehicleModel.setTipo(tipo);

        return vehicleModel;
    }

    public static String escogerTipo(int tipoVehiculo){
        String[] tiposVehiculos = {"Van", "Camión", "Pick up"};
        return tiposVehiculos[tipoVehiculo];
    }

    public static String escogerMarca(int marcaVehiculo){
        String[] marcaVehiculos = {"Chevrolet", "Renault", "Mazda", "Toyota", "Hiundai", "Kia"};
        return marcaVehiculos[marcaVehiculo];
    }

}
