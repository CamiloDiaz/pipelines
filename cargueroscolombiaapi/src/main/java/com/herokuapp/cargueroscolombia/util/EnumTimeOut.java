package com.herokuapp.cargueroscolombia.util;

public enum EnumTimeOut {

    ZERO(0),
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    SEVENTEEN(17),
    EIGHTEEN(18),
    FIFTY(50),
    NINETY_NINE(99),
    ONE_HUNDRED(100),
    NINE_HOUNDRED_NINETY_NINE(999),
    TWO_THOUSAND(2000),
    TWO_THOUSAND_AND_TWENTY_THREE(2023),
    NINETEEN_NINETY_NINE(1999),
    MAX_CAPACITY(99999),
    MIN_NUM(300000000),
    MAX_NUM(399999999),
    MIN_ID_NUM(100000000),
    MAX_ID_NUM(199999999),
    MIN_CAPACITY_FAIL(100000),
    MAX_CAPACITY_FAIL(999999);

    private final int value;

    EnumTimeOut(int value) {
        this.value = value;
    }

    public int getValue() {

        return value;
    }
}
