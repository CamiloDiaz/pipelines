package com.herokuapp.cargueroscolombia.util;

import com.github.javafaker.Faker;
import com.herokuapp.cargueroscolombia.models.getmodel.Conductor;
import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;

import static com.herokuapp.cargueroscolombia.util.Dictionary.SPACE_STRING;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.*;

public class VehicleModelDataFail {

    private VehicleModelDataFail() {
        throw new AssertionError("Instantiating utility class.");
    }

    public static void failAnioVehiculo(Vehiculo vehicleModel){
        Faker faker = new Faker();
        int anioFail = faker.number().numberBetween(ZERO.getValue(), NINETEEN_NINETY_NINE.getValue());
        vehicleModel.setAnio(anioFail);
    }

    public static void failCapacidadVehiculo(Vehiculo vehicleModel){
        Faker faker = new Faker();
        int capacidadFail= faker.number().numberBetween(MIN_CAPACITY_FAIL.getValue(), MAX_CAPACITY_FAIL.getValue());
        vehicleModel.setCapacidad(capacidadFail);
    }

    public static void failCelularConductor(Conductor conductor){
        Faker faker = new Faker();
        String celular= String.valueOf(faker.number().numberBetween(MIN_NUM.getValue(), MAX_NUM.getValue()));
        conductor.setCelular(celular);
    }

    public static void failMarcaVehiculo(Vehiculo vehicleModel){
        Faker faker = new Faker();
        int marcaVehiculo = faker.number().numberBetween(ZERO.getValue(), SIX.getValue());
        String marcaFail = escogerMarcaFail(marcaVehiculo);
        vehicleModel.setMarca(marcaFail);
    }

    public static void failPlacaVehiculo(Vehiculo vehicleModel){
        Faker faker = new Faker();
        String numerosPlaca = String.valueOf(faker.number().numberBetween(ONE_HUNDRED.getValue(), NINE_HOUNDRED_NINETY_NINE.getValue()));
        String letrasFail = faker.letterify("???");
        String placaFail = letrasFail.concat(numerosPlaca);
        vehicleModel.setPlaca(placaFail);
    }

    public static void failTipoVehiculo(Vehiculo vehicleModel){
        Faker faker = new Faker();
        String tipoFail = faker.cat().name();
        vehicleModel.setTipo(tipoFail);
    }

    public static void failCorreoSinArrobaConductor(Conductor conductor){
        Faker faker = new Faker();
        String correoFailA= faker.name().firstName().concat("gmail.com");
        conductor.setCorreo(correoFailA);
    }

    public static void failCorreoSinPuntoConductor(Conductor conductor){
        Faker faker = new Faker();
        String correoFailDot=faker.name().firstName().concat("@gmailcom");
        conductor.setCorreo(correoFailDot);
    }

    public static void failIdConductor(Conductor conductor){
        Faker faker = new Faker();
        int numId = faker.number().numberBetween(MIN_ID_NUM.getValue(), MAX_ID_NUM.getValue());
        conductor.setId(numId);
    }

    public static void failNombreConductor(Conductor conductor){
        Faker faker = new Faker();
        conductor.setNombre(faker.name().firstName());
    }

    public static void failMinEdadConductor(Conductor conductor){
        Faker faker = new Faker();
        int edadFail = faker.number().numberBetween(ZERO.getValue(), SEVENTEEN.getValue());
        conductor.setEdad(edadFail);
    }

    public static void failMaxEdadConductor(Conductor conductor){
        Faker faker = new Faker();
        int edadFailMax = faker.number().numberBetween(ONE_HUNDRED.getValue(), NINE_HOUNDRED_NINETY_NINE.getValue());
        conductor.setEdad(edadFailMax);
    }

    public static String escogerMarcaFail(int marcaVehiculo){
        String[] marcaVehiculos = {"Ch", "Re", "Ma", "To", "Hi", "Ki"};
        return marcaVehiculos[marcaVehiculo];
    }




}
