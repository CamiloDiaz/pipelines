package com.herokuapp.cargueroscolombia.question.getvehiculos;


import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class getQuestion implements Question<Vehiculo[]>  {

    @Override
    public Vehiculo[] answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(Vehiculo[].class);
    }
}

