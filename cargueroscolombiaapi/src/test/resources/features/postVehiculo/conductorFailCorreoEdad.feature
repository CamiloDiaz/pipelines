# language:es

Característica: Informacion equivocada del conductor

  Escenario: correo
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa el correo sin punto
    Cuando genere una consulta erronea con el campo correo sin punto
    Entonces el recurso debera responder codigo de estado fallido o bad request a correo sin punto

  Escenario: edad
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa edad mayor a cien
    Cuando cuando haga la peticion con edad mayor a cien
    Entonces el recurso debera responder codigo de estado fallido o bad request edad mayor a cien

