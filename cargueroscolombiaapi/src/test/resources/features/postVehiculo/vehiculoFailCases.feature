# language:es

Característica: Informacion equivocada del vehiculo

  Escenario: Placa
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa uno mal
    Cuando genere una consulta erronea con el campo placa
    Entonces el recurso debera responder codigo de estado fallido o bad request

  Escenario: Marca
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa marca mal
    Cuando cuando haga la peticion con esa marca
    Entonces el recurso debera responder codigo de estado fallido o bad request marca

  Escenario: Tipo
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa tipo mal
    Cuando cuando haga la peticion con esa tipo
    Entonces el recurso debera responder codigo de estado fallido o bad request tipo

  Escenario: Anio
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa anio mal
    Cuando cuando haga la peticion con esa anio
    Entonces el recurso debera responder codigo de estado fallido o bad request anio

  Escenario: Capacidad
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa capacidad mal
    Cuando cuando haga la peticion con esa capacidad
    Entonces el recurso debera responder codigo de estado fallido o bad request capacidad
