# language:es

Característica: Informacion equivocada del conductor

  Escenario: correo
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa el correo mal
    Cuando genere una consulta erronea con el campo correo
    Entonces el recurso debera responder codigo de estado fallido o bad request correo

  Escenario: celular
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa celular mal
    Cuando cuando haga la peticion con esa celular
    Entonces el recurso debera responder codigo de estado fallido o bad request celular

  Escenario: id
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa id mal
    Cuando cuando haga la peticion con esa id
    Entonces el recurso debera responder codigo de estado fallido o bad request id

  Escenario: nombre
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa nombre mal
    Cuando cuando haga la peticion con esa nombre
    Entonces el recurso debera responder codigo de estado fallido o bad request nombre

  Escenario: edad
    Dado que el conductor se encuentra en el recurso indicando sus datos e ingresa edad mal
    Cuando cuando haga la peticion con esa edad
    Entonces el recurso debera responder codigo de estado fallido o bad request edad

