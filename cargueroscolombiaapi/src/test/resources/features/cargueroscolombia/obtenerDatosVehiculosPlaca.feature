# language:es

Característica: Obtener datos de vehiculos por placa
  Yo como cliente de la aplicacion web
  Quiero obtener los datos de un vehiculo registrado por placa
  Para requerir un servicio


  Escenario: Adquirir datos de vehiculo por placa
    Dado El usuario desea ver los datos de vehiculos por placa
    Cuando  El usuario logra ver los datos de los vehiculos
    Entonces  El usuario podra obtener un mensaje exitoso
