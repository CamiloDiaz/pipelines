# language:es

Característica: Obtener datos de vehiculos
  Yo como cliente de la aplicacion web
  Quiero obtener los datos de todos los vehiculos registrados
  Para requerir un servicio


  Escenario: obtener datos de todos los vehiculos
    Dado El usuario desea ver los datos
    Cuando  El usuario logra ver todos los datos de los vehiculos y su conductor
    Entonces  El usuario recibira un mensaje exitoso
