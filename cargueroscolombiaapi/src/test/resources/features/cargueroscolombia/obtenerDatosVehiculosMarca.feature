# language:es

Característica: Obtener datos de vehiculos por marca
  Yo como cliente de la aplicacion web
  Quiero obtener los datos de un vehiculo registrado ingresando la marca
  Para requerir un servicio


  Escenario: Buscar datos de vehiculo por marca
    Dado El usuario desea ver los datos de vehiculos por marca
    Cuando  El usuario logra ver todos los datos de los vehiculos
    Entonces  El usuario obtendra un mensaje exitoso
