# language:es

Característica: Obtener datos de vehiculos
  Yo como cliente de la aplicacion web
  Quiero obtener los datos asosiados al correo de un conductor
  Para visualizar su informacion


  Escenario: obtener datos del conductor por correo
    Dado El usuario quiere ver los datos
    Cuando  El usuario hace la peticion, con el correo y recibe los datos asociados
    Entonces  El usuario recibira un mensaje exitoso y los datos
