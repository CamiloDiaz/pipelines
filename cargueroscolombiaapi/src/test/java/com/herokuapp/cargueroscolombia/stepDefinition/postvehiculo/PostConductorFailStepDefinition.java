package com.herokuapp.cargueroscolombia.stepDefinition.postvehiculo;

import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static com.herokuapp.cargueroscolombia.task.DoPost.doPost;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.VehicleModelData.vehicleModelInformation;
import static com.herokuapp.cargueroscolombia.util.VehicleModelDataFail.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class PostConductorFailStepDefinition extends Settings {

    private static final Logger LOGGER = Logger.getLogger(PostConductorFailStepDefinition.class);

    private final Actor actor = Actor.named("Conductor");
    private String bodyRequestFail;
    private Vehiculo vehiculo;

    //correo
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa el correo mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaElCorreoMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failCorreoSinArrobaConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("genere una consulta erronea con el campo correo")
    public void genereUnaConsultaErroneaConElCampoCorreo() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request correo")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoOBadRequestCorreo() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //celular
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa celular mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaCelularMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failCelularConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa celular")
    public void cuandoHagaLaPeticionConEsaCelular() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request celular")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestCelular() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //id
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa id mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaIdMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failIdConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa id")
    public void cuandoHagaLaPeticionConEsaId() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request id")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestId() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //nombre
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa nombre mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaNombreMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failNombreConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa nombre")
    public void cuandoHagaLaPeticionConEsaNombre() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request nombre")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestNombre() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //edad
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa edad mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaEdadMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failMinEdadConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa edad")
    public void cuandoHagaLaPeticionConEsaEdad() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request edad")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestEdad() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
}
