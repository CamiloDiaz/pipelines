package com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting;

import net.serenitybdd.screenplay.Actor;

public class Settings {
    protected static final String URL_BASE = "https://transporte-carga-back.herokuapp.com";
    protected static final String RESOURCE_POST = "/vehiculo";
    protected static final String RESOURCE_GET_VEHICULOS = "/vehiculos";
    protected static final String RESOURCE_MARCA = "/vehiculo/marca/";
    protected static final String RESOURCE_CORREO = "/conductor/correo/";
    protected static final String RESOURCE_PLACA = "/vehiculo/";
    protected final Actor actor = Actor.named("Tester");


}
