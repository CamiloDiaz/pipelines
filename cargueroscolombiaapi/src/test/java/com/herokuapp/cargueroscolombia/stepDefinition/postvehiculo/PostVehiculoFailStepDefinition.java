package com.herokuapp.cargueroscolombia.stepDefinition.postvehiculo;

import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static com.herokuapp.cargueroscolombia.task.DoPost.doPost;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.VehicleModelData.vehicleModelInformation;
import static com.herokuapp.cargueroscolombia.util.VehicleModelDataFail.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class PostVehiculoFailStepDefinition extends Settings {

    private static final Logger LOGGER = Logger.getLogger(PostVehiculoFailStepDefinition.class);

    private final Actor actor = Actor.named("Conductor");
    private String bodyRequestFail;
    private Vehiculo vehiculo;

    //PLACA
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa uno mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaUnoMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failPlacaVehiculo(vehiculo);
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("genere una consulta erronea con el campo placa")
    public void genereUnaConsultaErroneaConElCampoPlaca() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoOBadRequest() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //MARCA
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa marca mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaMarcaMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failMarcaVehiculo(vehiculo);
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa marca")
    public void cuandoHagaLaPeticionConEsaMarca() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request marca")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestMarca() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //TIPO
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa tipo mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaTipoMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failTipoVehiculo(vehiculo);
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa tipo")
    public void cuandoHagaLaPeticionConEsaTipo() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request tipo")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestTipo() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //ANIO
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa anio mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaAnioMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failAnioVehiculo(vehiculo);
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa anio")
    public void cuandoHagaLaPeticionConEsaAnio() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request anio")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestAnio() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //CAPACIDAD
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa capacidad mal")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaCapacidadMal() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failCapacidadVehiculo(vehiculo);
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con esa capacidad")
    public void cuandoHagaLaPeticionConEsaCapacidad() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request capacidad")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoODadRequestCapacidad() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
}
