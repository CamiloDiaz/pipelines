package com.herokuapp.cargueroscolombia.stepDefinition.postvehiculo;

import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static com.herokuapp.cargueroscolombia.task.DoPost.doPost;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.VehicleModelData.vehicleModelInformation;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class PostVehiculoTestStepDefinition extends Settings {

    private static final Logger LOGGER = Logger.getLogger(PostVehiculoTestStepDefinition.class);

    private final Actor actor = Actor.named("Conductor");
    private String bodyRequest;
    private Vehiculo vehiculo;

    @Dado("que el conductor se encuentra en el recurso indicando sus datos")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatos() {

        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        bodyRequest = vehiculo.toJson();
        LOGGER.info(bodyRequest);
    }

    @Cuando("genere una consulta para la creacion de su usuario")
    public void genereUnaConsultaParaLaCreacionDeSuUsuario() {

        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequest)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder con sus datos y un estado exitoso")
    public void elRecursoDeberaResponderConSusDatosYUnEstadoExitoso() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_CREATED,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_CREATED)
                )
        );

    }
}
