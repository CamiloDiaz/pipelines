package com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia;


import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.question.getvehiculos.getQuestion;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static com.herokuapp.cargueroscolombia.task.dogetvehiculos.DoGet.doGet;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.USER_DIR;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;

public class obtenerDatosVehiculosStep extends Settings {
    private static final Logger LOGGER = Logger.getLogger(obtenerDatosVehiculosStep.class);
    @Dado("El usuario desea ver los datos")
    public void elUsuarioDeseaVerLosDatos() {
        try {
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.whoCan(
                    CallAnApi.at(URL_BASE)
            );
        }catch (Exception exception){
            LOGGER.warn(exception.getMessage());
        }

    }

    @Cuando("El usuario logra ver todos los datos de los vehiculos y su conductor")
    public void elUsuarioLograVerTodosLosDatosDeLosVehiculosYSuConductor() {

        try{

            actor.attemptsTo(
                    doGet().usingTheResource(RESOURCE_GET_VEHICULOS)
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }

    @Entonces("El usuario recibira un mensaje exitoso")
    public void elUsuarioRecibiraUnMensajeExitoso() {

        try{
            LastResponse.received().answeredBy(actor).prettyPrint();

            Vehiculo[] Customer = new getQuestion().answeredBy(actor);

            actor.should(
                    seeThatResponse("codigo de respuesta: "+ HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK) )
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}
