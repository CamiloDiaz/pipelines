package com.herokuapp.cargueroscolombia.stepDefinition.postvehiculo;

import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static com.herokuapp.cargueroscolombia.task.DoPost.doPost;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.VehicleModelData.vehicleModelInformation;
import static com.herokuapp.cargueroscolombia.util.VehicleModelDataFail.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class PostConductorFailCorreoEdadStepDefinition extends Settings {

    private static final Logger LOGGER = Logger.getLogger(PostConductorFailCorreoEdadStepDefinition.class);

    private final Actor actor = Actor.named("Conductor");
    private String bodyRequestFail;
    private Vehiculo vehiculo;

    //correo
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa el correo sin punto")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaElCorreoSinPunto() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failCorreoSinPuntoConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("genere una consulta erronea con el campo correo sin punto")
    public void genereUnaConsultaErroneaConElCampoCorreoSinPunto() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request a correo sin punto")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoOBadRequestACorreoSinPunto() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
    //edad
    @Dado("que el conductor se encuentra en el recurso indicando sus datos e ingresa edad mayor a cien")
    public void queElConductorSeEncuentraEnElRecursoIndicandoSusDatosEIngresaEdadMayorACien() {
        PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URL_BASE));
        vehiculo = vehicleModelInformation();
        failMaxEdadConductor(vehiculo.getConductor());
        bodyRequestFail = vehiculo.toJson();
        LOGGER.info(bodyRequestFail);
    }

    @Cuando("cuando haga la peticion con edad mayor a cien")
    public void cuandoHagaLaPeticionConEsaEdadMayorACien() {
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequestFail)
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el recurso debera responder codigo de estado fallido o bad request edad mayor a cien")
    public void elRecursoDeberaResponderCodigoDeEstadoFallidoOBadRequestEdadMayorACien() {

        LastResponse.received().answeredBy(actor).prettyPrint();

        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_BAD_REQUEST,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_BAD_REQUEST)
                )
        );
    }
}
