package com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia;

import com.herokuapp.cargueroscolombia.models.getmodel.Vehiculo;
import com.herokuapp.cargueroscolombia.question.getvehiculos.getQuestion;
import com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia.setting.Settings;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


import static com.herokuapp.cargueroscolombia.task.DoPost.doPost;
import static com.herokuapp.cargueroscolombia.task.dogetvehiculos.DoGet.doGet;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.herokuapp.cargueroscolombia.util.Log4jValues.USER_DIR;
import static com.herokuapp.cargueroscolombia.util.VehicleModelData.vehicleModelInformation;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;

public class obtenerDatosVehiculoMarcaStep extends Settings {
    private static final Logger LOGGER = Logger.getLogger(obtenerDatosVehiculosStep.class);

    private final Actor actor = Actor.named("Conductor");
    private String bodyRequest;
    private Vehiculo vehiculo;

    @Dado("El usuario desea ver los datos de vehiculos por marca")
    public void elUsuarioDeseaVerLosDatosDeVehiculosPorMarca() {
        try {
            PropertyConfigurator.configure(System.getProperty("user.dir")+"/"+LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.can(CallAnApi.at(URL_BASE));
            vehiculo = vehicleModelInformation();
            bodyRequest = vehiculo.toJson();
            LOGGER.info(bodyRequest);


            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.whoCan(
                    CallAnApi.at(URL_BASE)
            );
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE_POST)
                            .andBodyRequest(bodyRequest)
            );
        }catch (Exception exception){
            LOGGER.warn(exception.getMessage());
        }

    }

    @Cuando("El usuario logra ver todos los datos de los vehiculos")
    public void elUsuarioLograVerTodosLosDatosDeLosVehiculos() {
        try{
            actor.attemptsTo(
                    doGet().usingTheResource(RESOURCE_MARCA+vehiculo.getMarca())
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }

    @Entonces("El usuario obtendra un mensaje exitoso")
    public void elUsuarioObtendraUnMensajeExitoso() {
        try{
            LastResponse.received().answeredBy(actor).prettyPrint();

            Vehiculo[] Customer = new getQuestion().answeredBy(actor);

            actor.should(
                    seeThatResponse("codigo de respuesta: "+ HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK) ),

                    seeThat(
                            "El id de usuario es :", x -> Customer[0].getMarca(), equalTo(vehiculo.getMarca()))
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }


}
