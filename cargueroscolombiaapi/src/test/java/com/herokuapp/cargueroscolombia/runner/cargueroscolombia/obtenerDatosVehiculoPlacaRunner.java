package com.herokuapp.cargueroscolombia.runner.cargueroscolombia;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/cargueroscolombia/obtenerDatosVehiculosPlaca.feature"},
        glue = {"com.herokuapp.cargueroscolombia.stepDefinition.cargueroscolombia"}
)

public class obtenerDatosVehiculoPlacaRunner {
}
