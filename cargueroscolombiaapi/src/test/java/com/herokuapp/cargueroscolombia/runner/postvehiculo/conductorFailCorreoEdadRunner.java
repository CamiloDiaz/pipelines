package com.herokuapp.cargueroscolombia.runner.postvehiculo;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/postVehiculo/conductorFailCorreoEdad.feature"},
        glue = {"com.herokuapp.cargueroscolombia.stepDefinition.postvehiculo"}
)

public class conductorFailCorreoEdadRunner {
}
