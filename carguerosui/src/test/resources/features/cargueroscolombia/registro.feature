# language:es

Característica: Registrar datos
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de informacion
Dado El usuario se encuentra en el inicio de la pagina principal
Cuando  El usuario ingresa al link de registro y procede a ingresar sus datos
Entonces  El usuario obtiene un mensaje de registro exitoso

