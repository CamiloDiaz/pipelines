# language:es

Característica: Registrar placa y marca incorrecta
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de placa por letras incorrecta
Dado El usuario procede a estar en el inicio de la aplicacion web
Cuando  El usuario ingresa al link de registro e ingresa solo una letra en la placa
Entonces  El usuario obtiene un mensaje de registro erroneo por placa

  Escenario: Registro de numero de placa incorrecta
    Dado El usuario procede a estar en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa un solo numero en la placa por numero
    Entonces  El usuario obtiene un mensaje erroneo por numero de placa incorrecta

  Escenario: Registro marca de vehiculo incorrecto
    Dado El usuario procede a estar en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa todos los datos sin registrar la marca del vehiculo
    Entonces  El usuario obtiene un mensaje erroneo por la marca incorrecto



