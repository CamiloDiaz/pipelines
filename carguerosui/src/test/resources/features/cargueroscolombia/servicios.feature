# language:es

Característica: Solicitar un servicio
  Yo como cliente de la empresa CarguerosColombia
  Quiero poder acceder a la información de los conductores
  Para poder contactarlos y solicitar un servicio de carga

  Escenario: Seleccionar un vehiculo
    Dado que el cliente se encuentra en la pagina de inicio de CarguerosColombia
    Cuando  ingreso a la seccion de servicios y selecciono un tipo de vehiculo
    Entonces deberia observar la informacion de contacto del conductor de dicho vehiculo