# language: es
Característica: Inicio de sesion fallida por correo
  Yo como transportador de mercancía
  Quiero iniciar sesion en la plataforma
  Para hacer uso del aplicativo y ofrecer mis servicios.

  Escenario: Inicio de sesion con correo incorrecto
    Dado Que me encuentro en la pagina principal de la aplicacion
    Cuando ingreso mis credenciales con un correo no valido
    Entonces debo observar un mensaje de error por correo no valido

  Escenario: Inicio de sesion sin correo
    Dado Que me encuentro en la pagina principal de la aplicacion
    Cuando ingreso mis credenciales menos el correo
    Entonces debo observar un mensaje de error por falta de correo