# language:es

Característica: Registrar correo, clave y conformacion de clave incorrectos
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de correo incorrecto
Dado El usuario se posiciona en el inicio de la aplicacion web
Cuando  El usuario ingresa al link de registro e ingresa un correo que no cumple con el formato permitido
Entonces  El usuario obtiene un mensaje de registro erroneo por correo incorrecto

  Escenario: Registro de clave sin letra mayuscula
    Dado El usuario se posiciona en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa una clave que no contiene letra mayuscula
    Entonces  El usuario obtiene un mensaje erroneo por clave incorrecta

  Escenario: Registro de clave cantidad de caracteres menor al permitido
    Dado El usuario se posiciona en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa una clave con una cantidad de caracteres menor a la permitida
    Entonces  El usuario obtiene un mensaje erroneo por clave con cantidad de caracteres invalido

  Escenario: Registro de confirmacion de clave incorrecta
    Dado El usuario se posiciona en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa una clave diferente para la confirmacion de la clave
    Entonces  El usuario obtiene un mensaje erroneo por clave no compatible


