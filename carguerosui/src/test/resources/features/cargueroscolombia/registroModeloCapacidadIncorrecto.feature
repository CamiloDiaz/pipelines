# language:es

Característica: Registrar Modelo y Capacidad incorrecta
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de valor de modelo por debajo del valor permitido
Dado El usuario esta en el inicio de la aplicacion web
Cuando  El usuario ingresa al link de registro e ingresa un modelo por debajo del valor permitido
Entonces  El usuario obtiene un mensaje de registro erroneo por valor incorrecto del modelo

  Escenario: Registro de valor de modelo por encima del valor permitido
    Dado El usuario esta en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa un modelo por encima del valor permitido
    Entonces  El usuario obtiene un mensaje erroneo por valor invalido del modelo

  Escenario: Registro capacidad incorrecto
    Dado El usuario esta en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa una capacidad igual a cero o no ingresar nada
    Entonces  El usuario obtiene un mensaje erroneo por la capacidad incorrecta



