# language: es
Característica: Inicio de sesion
  Yo como transportador de mercancía
  Quiero iniciar sesion en la plataforma
  Para hacer uso del aplicativo y ofrecer mis servicios.

  Escenario: Inicio de sesion exitoso
    Dado Que me encuentro navegando en la pagina principal de la aplicacion
    Y Que ya me he registrado en la aplicacion web
    Cuando ingreso mis credenciales correctamente
    Entonces deberia observar un perfil de usuario con mis datos