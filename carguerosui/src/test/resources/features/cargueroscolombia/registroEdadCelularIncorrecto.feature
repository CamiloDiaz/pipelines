# language:es

Característica: Registrar edad y celular incorrectos
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de Edad menor de la permitida
Dado El usuario se encuentra en el inicio de la aplicacion web
Cuando  El usuario ingresa al link de registro e ingresa una edad menor de la permitida
Entonces  El usuario obtiene un mensaje de registro erroneo por no alcanzar la edad permitida

  Escenario: Registro de Edad mayor de la permitida
    Dado El usuario se encuentra en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa una edad mayor de la permitida
    Entonces  El usuario obtiene un mensaje erroneo por sobrepasar la edad permitida

  Escenario: Registro de celular vacio
    Dado El usuario se encuentra en el inicio de la aplicacion web
    Cuando  El usuario ingresa al link de registro e ingresa todos los datos sin ingresar el numero de celular
    Entonces  El usuario obtiene un mensaje erroneo por campo vacio del celular


