# language:es

Característica: Registrar id y nombre incorrectos
  Yo como transportador de mercancía
  Quiero registrarme en la aplicacion web
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Registro de nombre vacio
Dado El usuario se encuentra en el inicio de la aplicacion
Cuando  El usuario ingresa al link de registro e ingresa los datos sin ingresar su nombre completo
Entonces  El usuario obtiene un mensaje de registro erroneo de nombre vacio

  Escenario: Registro de id vacio
    Dado El usuario se encuentra en el inicio de la aplicacion
    Cuando  El usuario ingresa al link de registro e ingresa los datos sin ingresar su id
    Entonces  El usuario obtiene un mensaje erroneo de id vacio

  Escenario: Registro de id menor al valor permitido
    Dado El usuario se encuentra en el inicio de la aplicacion
    Cuando  El usuario ingresa al link de registro e ingresa un id menor al valor permitido
    Entonces  El usuario obtiene un mensaje erroneo de id invalido


