# language:es

Característica: inicio de sesion contraseña incorrecta
  Yo como transportador de mercancía
  Quiero iniciar sesion en la plataforma
  Para hacer uso del aplicativo y ofrecer mis servicios.

Escenario: Clave Incorrecta
Dado El usuario se encuentra en la pagina principal
Cuando  El usuario ingresa el correo y una clave incorrecta
Entonces  El usuario obtiene un mensaje de error

Escenario: Clave vacia
  Dado El usuario se encuentra en la pagina principal
  Cuando El usuario ingresa el correo y no ingresa clave
  Entonces El usuario obtiene un mensaje de error indicando que no ingreso clave.

