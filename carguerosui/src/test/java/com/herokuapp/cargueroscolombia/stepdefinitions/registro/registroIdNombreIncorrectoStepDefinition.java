package com.herokuapp.cargueroscolombia.stepdefinitions.registro;

import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.registro.registroIdIncorrectoQuestion.registroIdIncorrectoQuestion;
import static com.herokuapp.cargueroscolombia.question.registro.registroNombreIncorrectoQuestion.registroCasosIncorrectosQuestion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class registroIdNombreIncorrectoStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(registroIdNombreIncorrectoStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String mensajeRegistroNombreIncorrecto = "Ingrese 1 nombre y por lo menos 1 apellido";
    private static final String mensajeRegistroIdIncorrecto = "Ingrese un ID válido";
    private DatosRegistroModel random;

    @Dado("El usuario se encuentra en el inicio de la aplicacion")
    public void elUsuarioSeEncuentraEnElInicioDeLaAplicacion() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa los datos sin ingresar su nombre completo")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaLosDatosSinIngresarSuNombreCompleto() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro("")
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de registro erroneo de nombre vacio")
    public void elUsuarioObtieneUnMensajeDeRegistroErroneoDeNombreVacio() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroNombreIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }
    @Cuando("El usuario ingresa al link de registro e ingresa los datos sin ingresar su id")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaLosDatosSinIngresarSuId() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro("")
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }


    }
    @Entonces("El usuario obtiene un mensaje erroneo de id vacio")
    public void elUsuarioObtieneUnMensajeErroneoDeIdVacio() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroIdIncorrectoQuestion()
                                    .validacionRegistro(mensajeRegistroIdIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa un id menor al valor permitido")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnIdMenorAlValorPermitido() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getIdIncorrecto())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo de id invalido")
    public void elUsuarioObtieneUnMensajeErroneoDeIdInvalido() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroIdIncorrectoQuestion()
                                    .validacionRegistro(mensajeRegistroIdIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }


}
