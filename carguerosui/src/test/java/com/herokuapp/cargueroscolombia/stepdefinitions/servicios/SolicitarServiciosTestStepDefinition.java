package com.herokuapp.cargueroscolombia.stepdefinitions.servicios;

import com.herokuapp.cargueroscolombia.model.service.ServiceModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.service.VehicleInformation.vehicleInformationValidatedWithInformationBox;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.servicios.BrowseToServicePage.browseToServicePage;
import static com.herokuapp.cargueroscolombia.task.servicios.SelectVehicle.selectVehicle;
import static com.herokuapp.cargueroscolombia.util.Dictionary.*;
import static com.herokuapp.cargueroscolombia.util.ServiceModelData.serviceModelInformation;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class SolicitarServiciosTestStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(SolicitarServiciosTestStepDefinition.class);
    private static final String ACTOR_NAME = "Cliente";
    private static final ServiceModel serviceModelData = serviceModelInformation();

    @Dado("que el cliente se encuentra en la pagina de inicio de CarguerosColombia")
    public void queElClienteSeEncuentraEnLaPaginaDeInicioDeCarguerosColombia() {

        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            LOGGER.warn(OPEN_LANDING_PAGE_VALIDATION_ERROR);
        }
        LOGGER.info(OPEN_LANDING_PAGE_DONE);
    }

    @Cuando("ingreso a la seccion de servicios y selecciono un tipo de vehiculo")
    public void ingresoALaSeccionDeServiciosYSeleccionoUnTipoDeVehiculo() {
        try{
            theActorInTheSpotlight().attemptsTo(
                    browseToServicePage(),
                    selectVehicle()
                            .choosingAnOptionOfVehicle(serviceModelData.getVehicleSelection())
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
            LOGGER.warn(SELECT_VEHICLE_VALIDATION_ERROR);
        }
        LOGGER.info(SELECT_VEHICLE_DONE);
    }

    @Entonces("deberia observar la informacion de contacto del conductor de dicho vehiculo")
    public void deberiaObservarLaInformacionDeContactoDelConductorDeDichoVehiculo() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            vehicleInformationValidatedWithInformationBox()
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            LOGGER.warn(DRIVER_INFO_VALIDATION_ERROR);
        }
        LOGGER.info(DRIVER_INFO_DONE);
    }

}
