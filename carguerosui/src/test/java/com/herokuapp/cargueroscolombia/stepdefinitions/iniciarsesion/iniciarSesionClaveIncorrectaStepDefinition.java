package com.herokuapp.cargueroscolombia.stepdefinitions.iniciarsesion;

import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.iniciarsesion.iniciarSesionClaveIncorrecta.claveIncorrecta;
import static com.herokuapp.cargueroscolombia.task.iniciarsesion.iniciarSesion.sesion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;


public class iniciarSesionClaveIncorrectaStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(iniciarSesionClaveIncorrectaStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String ValidationMessage = "Password equivocado";
    private static final String ValidationEmptyMessage = "La contraseña no puede ser vacia";

    @Dado("El usuario se encuentra en la pagina principal")
    public void elUsuarioSeEncuentraEnLaPaginaPrincipal() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Cuando("El usuario ingresa el correo y una clave incorrecta")
    public void elUsuarioIngresaElCorreoYUnaClaveIncorrecta() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    sesion()
                            .correoIniciarSesion(CORREO_INICIAR_SESION)
                            .claveIniciarSesion(CLAVE_INCORRECTA)

            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de error")
    public void elUsuarioObtieneUnMensajeDeError() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            claveIncorrecta()
                                    .wasErrorLogInUser(ValidationMessage) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa el correo y no ingresa clave")
    public void elUsuarioIngresaElCorreoYNoIngresaClave() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    sesion()
                            .correoIniciarSesion(CORREO_INICIAR_SESION)
                            .claveIniciarSesion(EMPTY_STRING)

            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Entonces("El usuario obtiene un mensaje de error indicando que no ingreso clave.")
    public void elUsuarioObtieneUnMensajeDeErrorIndicandoQueNoIngresoClave() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            claveIncorrecta()
                                    .wasErrorLogInUser(ValidationEmptyMessage) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

}
