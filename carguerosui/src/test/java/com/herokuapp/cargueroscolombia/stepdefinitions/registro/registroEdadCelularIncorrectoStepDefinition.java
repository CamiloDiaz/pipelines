package com.herokuapp.cargueroscolombia.stepdefinitions.registro;

import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.registro.registroNombreIncorrectoQuestion.registroCasosIncorrectosQuestion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class registroEdadCelularIncorrectoStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(registroEdadCelularIncorrectoStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String mensajeRegistroEdadIncorrecto = "Ingrese una edad correcta > 17 < 66";
    private static final String mensajeRegistroCelularVacio = "Ingrese un numero de contacto válido";
    private DatosRegistroModel random;

    @Dado("El usuario se encuentra en el inicio de la aplicacion web")
    public void elUsuarioSeEncuentraEnElInicioDeLaAplicacionWeb() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa una edad menor de la permitida")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaEdadMenorDeLaPermitida() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdadMenor())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de registro erroneo por no alcanzar la edad permitida")
    public void elUsuarioObtieneUnMensajeDeRegistroErroneoPorNoAlcanzarLaEdadPermitida() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroEdadIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa una edad mayor de la permitida")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaEdadMayorDeLaPermitida() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdadMayor())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por sobrepasar la edad permitida")
    public void elUsuarioObtieneUnMensajeErroneoPorSobrepasarLaEdadPermitida() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroEdadIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa todos los datos sin ingresar el numero de celular")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaTodosLosDatosSinIngresarElNumeroDeCelular() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro("")
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }

    @Entonces("El usuario obtiene un mensaje erroneo por campo vacio del celular")
    public void elUsuarioObtieneUnMensajeErroneoPorCampoVacioDelCelular() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroCelularVacio) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }
}
