package com.herokuapp.cargueroscolombia.stepdefinitions.registro;

import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.registro.registroNombreIncorrectoQuestion.registroCasosIncorrectosQuestion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class registroModeloCapacidadIncorrectoStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(registroModeloCapacidadIncorrectoStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String mensajeRegistroModeloIncorrecto = "Ingrese año correcto de vehiculo";
    private static final String mensajeRegistroCapacidadIncorrecto = "Ingrese una capacidad correcta";
    private DatosRegistroModel random;

    @Dado("El usuario esta en el inicio de la aplicacion web")
    public void elUsuarioEstaEnElInicioDeLaAplicacionWeb() {

        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa un modelo por debajo del valor permitido")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnModeloPorDebajoDelValorPermitido() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(String.valueOf(NINETEEN_SEVENTY.getValue()))
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de registro erroneo por valor incorrecto del modelo")
    public void elUsuarioObtieneUnMensajeDeRegistroErroneoPorValorIncorrectoDelModelo() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroModeloIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa un modelo por encima del valor permitido")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnModeloPorEncimaDelValorPermitido() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(String.valueOf(TWO_THOUSAND_AND_TWENTY_FOUR.getValue()))
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por valor invalido del modelo")
    public void elUsuarioObtieneUnMensajeErroneoPorValorInvalidoDelModelo() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroModeloIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa una capacidad igual a cero o no ingresar nada")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaCapacidadIgualACeroONoIngresarNada() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(String.valueOf(ZERO.getValue()))
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por la capacidad incorrecta")
    public void elUsuarioObtieneUnMensajeErroneoPorLaCapacidadIncorrecta() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroCapacidadIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }


}
