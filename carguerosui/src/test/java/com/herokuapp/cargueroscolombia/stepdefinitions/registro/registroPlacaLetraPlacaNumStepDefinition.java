package com.herokuapp.cargueroscolombia.stepdefinitions.registro;

import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.registro.registroNombreIncorrectoQuestion.registroCasosIncorrectosQuestion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class registroPlacaLetraPlacaNumStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(registroPlacaLetraPlacaNumStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String mensajeRegistroPlacaLetrasIncorrecto = "Ingrese Letras de placa correctamente: Mínimo 3 y en mayúsculas";
    private static final String mensajeRegistroPlacaNumIncorrecto = "Ingrese numero de placa correcto";
    private static final String mensajeRegistroMarcaIncorrecto = "Ingrese marca del vehiculo";
    private DatosRegistroModel random;

    @Dado("El usuario procede a estar en el inicio de la aplicacion web")
    public void elUsuarioProcedeAEstarEnElInicioDeLaAplicacionWeb() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }


    @Cuando("El usuario ingresa al link de registro e ingresa solo una letra en la placa")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaSoloUnaLetraEnLaPlaca() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlacaIncorrecta())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de registro erroneo por placa")
    public void elUsuarioObtieneUnMensajeDeRegistroErroneoPorPlaca() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroPlacaLetrasIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa un solo numero en la placa por numero")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnSoloNumeroEnLaPlacaPorNumero() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlacaIncorrecto())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por numero de placa incorrecta")
    public void elUsuarioObtieneUnMensajeErroneoPorNumeroDePlacaIncorrecta() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroPlacaNumIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa todos los datos sin registrar la marca del vehiculo")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaTodosLosDatosSinRegistrarLaMarcaDelVehiculo() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro("")
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por la marca incorrecto")
    public void elUsuarioObtieneUnMensajeErroneoPorLaMarcaIncorrecto() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroMarcaIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }



}
