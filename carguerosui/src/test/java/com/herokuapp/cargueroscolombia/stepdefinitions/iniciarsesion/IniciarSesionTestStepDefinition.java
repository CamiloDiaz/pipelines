package com.herokuapp.cargueroscolombia.stepdefinitions.iniciarsesion;

import com.herokuapp.cargueroscolombia.exceptions.ValidationTextDoNotMatch;
import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.exceptions.ValidationTextDoNotMatch.VALIDATION_DO_NOT_MATCH;
import static com.herokuapp.cargueroscolombia.question.iniciarsesion.IniciarSesionExitosamente.iniciarSesionExitosamente;
import static com.herokuapp.cargueroscolombia.task.iniciarsesion.iniciarSesion.sesion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.userinterface.perfildeusuario.PerfilDeUsuarioPagina.*;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class IniciarSesionTestStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(IniciarSesionTestStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String ValidationSuccessfulMessage = "Cerrar sesión";
    private DatosRegistroModel random;

    @Dado("Que me encuentro navegando en la pagina principal de la aplicacion")
    public void queMeEncuentroNavegandoEnLaPaginaPrincipalDeLaAplicacion() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Dado("Que ya me he registrado en la aplicacion web")
    public void queYaMeHeRegistradoEnLaAplicacionWeb() {
        random = generarPersonasRandom();
        theActorInTheSpotlight().wasAbleTo(
                toRegistro(),
                registroDatos()
                        .nombreApellidoRegistro(random.getNombreCompleto())
                        .idRegistro(random.getId())
                        .edadRegistro(random.getEdad())
                        .celularRegistro(random.getCelular())
                        .correoRegistro(random.getCorreo())
                        .claveRegistro(random.getClave())
                        .confirmarClaveRegistro(random.getClave())
                        .placaRegistro(random.getPlaca())
                        .numeroPlacaRegistro(random.getNumeroPlaca())
                        .marcaRegistro(random.getMarca())
                        .modeloRegistro(random.getModelo())
                        .capacidadRegistro(random.getCapacidad())
                        .tipoVehiculos(random.getTipoVehiculo())
        );
    }
    @Cuando("ingreso mis credenciales correctamente")
    public void ingresoMisCredencialesCorrectamente() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    sesion()
                            .correoIniciarSesion(random.getCorreo())
                            .claveIniciarSesion(random.getClave())
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("deberia observar un perfil de usuario con mis datos")
    public void deberiaObservarUnPerfilDeUsuarioConMisDatos() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            iniciarSesionExitosamente()
                                    .fueLlenadoConElNombre(random.getNombreCompleto())
                                    .fueLlenadoConElID(random.getId())
                                    .fueLlenadoConElEmail(random.getCorreo())
                                    .fueLlenadoConLaEdad(random.getEdad())
                                    .fueLlenadoConElContacto(random.getCelular())
                                    .fueLlenadoConLaPlaca(random.getPlaca()+random.getNumeroPlaca())
                                    .fueLlenadoConLaMarca(random.getMarca())
                                    .fueLlenadoConElAnio(random.getModelo())
                                    .fueLlenadoConLaCapacidad(random.getCapacidad())
                                    .yFueLlenadoConElTipoDeVehiculo(random.getTipoVehiculo())
                                    .es(), equalTo(true)
                    )
                            .orComplainWith(ValidationTextDoNotMatch.class, String.format(
                                    VALIDATION_DO_NOT_MATCH, compareInWithSystemOutcome()))
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    private String compareInWithSystemOutcome(){
        return "\n" + "Data for test (expected) : System outcome (actual)"
                + "\n" + random.getNombreCompleto() + " : " + nombreCompleto.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getId() + " : " + id.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getCorreo() + " : " + email.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getEdad() + " : " + edad.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getCelular() + " : " + contacto.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getPlaca()+random.getNumeroPlaca() + " : " + placa.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getMarca() + " : " + marca.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getModelo() + " : " + anio.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getCapacidad() + " : " + capacidad.resolveFor(theActorInTheSpotlight()).getValue()
                + "\n" + random.getTipoVehiculo() + " : " + tipo.resolveFor(theActorInTheSpotlight()).getValue()
                ;
    }
}
