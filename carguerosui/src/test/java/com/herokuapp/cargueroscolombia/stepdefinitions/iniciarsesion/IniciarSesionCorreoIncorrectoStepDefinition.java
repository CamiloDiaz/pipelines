package com.herokuapp.cargueroscolombia.stepdefinitions.iniciarsesion;

import com.herokuapp.cargueroscolombia.exceptions.ValidationTextDoNotMatch;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.targets.Target;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.exceptions.ValidationTextDoNotMatch.VALIDATION_DO_NOT_MATCH;
import static com.herokuapp.cargueroscolombia.question.iniciarsesion.IniciarSesionCorreoIncorrecto.correoIncorrecto;
import static com.herokuapp.cargueroscolombia.task.iniciarsesion.iniciarSesion.sesion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.userinterface.iniciarsesion.iniciarSesion.mensajeDeError;
import static com.herokuapp.cargueroscolombia.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class IniciarSesionCorreoIncorrectoStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(IniciarSesionCorreoIncorrectoStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String ValidationMessageWithoutEmail = "El correo no puede ser vacio";
    private static final String ValidationMessageInvalidUser = "Usuario no funciona";

    @Dado("Que me encuentro en la pagina principal de la aplicacion")
    public void queMeEncuentroEnLaPaginaPrincipalDeLaAplicacion() {
        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Cuando("ingreso mis credenciales con un correo no valido")
    public void ingresoMisCredencialesConUnCorreoNoValido() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    sesion()
                            .correoIniciarSesion(CORREO_INCORRECTO)
                            .claveIniciarSesion(CLAVE_INICIAR_SESION)

            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("debo observar un mensaje de error por correo no valido")
    public void deboObservarUnMensajeDeErrorPorCorreoNoValido() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            correoIncorrecto()
                                    .wasErrorLogInUser(ValidationMessageInvalidUser) , equalTo(true)
                    )
                            .orComplainWith(ValidationTextDoNotMatch.class, String.format(
                                    VALIDATION_DO_NOT_MATCH, compareInWithSystemOutcome(
                                            ValidationMessageInvalidUser, mensajeDeError)))
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Cuando("ingreso mis credenciales menos el correo")
    public void ingresoMisCredencialesMenosElCorreo() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    sesion()
                            .correoIniciarSesion(EMPTY_STRING)
                            .claveIniciarSesion(CLAVE_INICIAR_SESION)

            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("debo observar un mensaje de error por falta de correo")
    public void deboObservarUnMensajeDeErrorPorFaltaDeCorreo() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            correoIncorrecto()
                                    .wasErrorLogInUser(ValidationMessageWithoutEmail) , equalTo(true)
                    )
                            .orComplainWith(ValidationTextDoNotMatch.class, String.format(
                                    VALIDATION_DO_NOT_MATCH, compareInWithSystemOutcome(
                                            ValidationMessageWithoutEmail, mensajeDeError)))
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    private String compareInWithSystemOutcome(String expectedMessage, Target target){
        return "\n" + "Data for test : System outcome"
                + "\n" + expectedMessage + " : " + target.resolveFor(theActorInTheSpotlight()).getText()
                ;
    }
}
