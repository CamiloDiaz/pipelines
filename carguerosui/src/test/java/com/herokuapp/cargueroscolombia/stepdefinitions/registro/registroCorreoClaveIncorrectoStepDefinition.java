package com.herokuapp.cargueroscolombia.stepdefinitions.registro;

import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;
import com.herokuapp.cargueroscolombia.stepdefinitions.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static com.herokuapp.cargueroscolombia.question.registro.registroNombreIncorrectoQuestion.registroCasosIncorrectosQuestion;
import static com.herokuapp.cargueroscolombia.task.landingpage.OpenLandingPage.openLandingPage;
import static com.herokuapp.cargueroscolombia.task.registro.BrowserToRegistro.toRegistro;
import static com.herokuapp.cargueroscolombia.task.registro.registro.registroDatos;
import static com.herokuapp.cargueroscolombia.util.DatosRegistro.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class registroCorreoClaveIncorrectoStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(registroCorreoClaveIncorrectoStepDefinition.class);
    private static final String ACTOR = "Admin";
    private static final String mensajeRegistroCorreoIncorrecto = "Ingrese un correo correcto";
    private static final String mensajeRegistroClaveIncorrecta = "Ingrese contraseña correcta. Al menos una mayuscula y minimo 8 caracteres";
    private static final String mensajeRegistroConfirmarClaveIncorrecta = "Confirmar contraseña correcta";

    private DatosRegistroModel random;

    @Dado("El usuario se posiciona en el inicio de la aplicacion web")
    public void elUsuarioSePosicionaEnElInicioDeLaAplicacionWeb() {

        try {
            actorSetupTheBrowser(ACTOR);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }


    @Cuando("El usuario ingresa al link de registro e ingresa un correo que no cumple con el formato permitido")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnCorreoQueNoCumpleConElFormatoPermitido() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreoIncorrecto())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClave())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje de registro erroneo por correo incorrecto")
    public void elUsuarioObtieneUnMensajeDeRegistroErroneoPorCorreoIncorrecto() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroCorreoIncorrecto) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("El usuario ingresa al link de registro e ingresa una clave que no contiene letra mayuscula")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaClaveQueNoContieneLetraMayuscula() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClaveSinMayuscula())
                            .confirmarClaveRegistro(random.getClaveSinMayuscula())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por clave incorrecta")
    public void elUsuarioObtieneUnMensajeErroneoPorClaveIncorrecta() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroClaveIncorrecta) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Cuando("El usuario ingresa al link de registro e ingresa una clave con una cantidad de caracteres menor a la permitida")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaClaveConUnaCantidadDeCaracteresMenorALaPermitida() {

        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClaveCantidadMenorCaracteres())
                            .confirmarClaveRegistro(random.getClaveCantidadMenorCaracteres())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("El usuario obtiene un mensaje erroneo por clave con cantidad de caracteres invalido")
    public void elUsuarioObtieneUnMensajeErroneoPorClaveConCantidadDeCaracteresInvalido() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroClaveIncorrecta) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }
    @Cuando("El usuario ingresa al link de registro e ingresa una clave diferente para la confirmacion de la clave")
    public void elUsuarioIngresaAlLinkDeRegistroEIngresaUnaClaveDiferenteParaLaConfirmacionDeLaClave() {
        try {
            random = generarPersonasRandom();
            theActorInTheSpotlight().attemptsTo(
                    toRegistro(),
                    registroDatos()
                            .nombreApellidoRegistro(random.getNombreCompleto())
                            .idRegistro(random.getId())
                            .edadRegistro(random.getEdad())
                            .celularRegistro(random.getCelular())
                            .correoRegistro(random.getCorreo())
                            .claveRegistro(random.getClave())
                            .confirmarClaveRegistro(random.getClaveSinMayuscula())
                            .placaRegistro(random.getPlaca())
                            .numeroPlacaRegistro(random.getNumeroPlaca())
                            .marcaRegistro(random.getMarca())
                            .modeloRegistro(random.getModelo())
                            .capacidadRegistro(random.getCapacidad())
                            .tipoVehiculos(random.getTipoVehiculo())
            );

        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }


    }
    @Entonces("El usuario obtiene un mensaje erroneo por clave no compatible")
    public void elUsuarioObtieneUnMensajeErroneoPorClaveNoCompatible() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            registroCasosIncorrectosQuestion()
                                    .validacionRegistro(mensajeRegistroConfirmarClaveIncorrecta) , equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }


}
