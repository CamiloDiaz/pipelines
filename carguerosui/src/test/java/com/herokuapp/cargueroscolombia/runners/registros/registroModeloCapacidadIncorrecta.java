package com.herokuapp.cargueroscolombia.runners.registros;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/cargueroscolombia/registroModeloCapacidadIncorrecto.feature"},
        glue = {"com.herokuapp.cargueroscolombia.stepdefinitions"}
)

public class registroModeloCapacidadIncorrecta {
}
