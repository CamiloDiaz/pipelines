package com.herokuapp.cargueroscolombia.runners.iniciarsesion;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/cargueroscolombia/iniciarSesionCorreoIncorrecto.feature"},
        glue = {"com.herokuapp.cargueroscolombia.stepdefinitions.iniciarsesion"}
)
public class IniciarSesionCorreoIncorrecto {
}
