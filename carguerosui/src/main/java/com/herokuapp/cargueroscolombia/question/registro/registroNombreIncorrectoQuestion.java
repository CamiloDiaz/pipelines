package com.herokuapp.cargueroscolombia.question.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.registro.registro.validacionRegistroIncorrecto;


public class registroNombreIncorrectoQuestion implements Question {
    private String registroNombreIncorrecto;

    public registroNombreIncorrectoQuestion validacionRegistro(String validation) {
        this.registroNombreIncorrecto = validation;
        return this;

    }
        public static registroNombreIncorrectoQuestion registroCasosIncorrectosQuestion(){
            return new registroNombreIncorrectoQuestion();
        }

    @Override
    public Boolean answeredBy(Actor actor) {
        return validacionRegistroIncorrecto.resolveFor(actor).containsOnlyText(registroNombreIncorrecto);
    }
}
