package com.herokuapp.cargueroscolombia.question.iniciarsesion;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static com.herokuapp.cargueroscolombia.userinterface.iniciarsesion.iniciarSesion.mensajeDeError;

public class iniciarSesionClaveIncorrecta implements Question {

    private String mensajeContraseñaError;

    public iniciarSesionClaveIncorrecta wasErrorLogInUser(String validation) {
        this.mensajeContraseñaError = validation;
        return this;

    }

    public static iniciarSesionClaveIncorrecta claveIncorrecta(){
        return new iniciarSesionClaveIncorrecta();
    }


    @Override
    public Object answeredBy(Actor actor) {
        return mensajeDeError.resolveFor(actor).containsOnlyText(mensajeContraseñaError);
    }
}
