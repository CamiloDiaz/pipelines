package com.herokuapp.cargueroscolombia.question.service;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.servicios.ServiciosPage.VEHICLE_DATA_VALIDATION;

public class VehicleInformation implements Question<Boolean> {


    @Override
    public Boolean answeredBy(Actor actor) {
        return VEHICLE_DATA_VALIDATION.resolveFor(actor).isVisible();
    }

    public VehicleInformation is(){
        return this;
    }

    public static VehicleInformation vehicleInformationValidatedWithInformationBox(){
        return new VehicleInformation();
    }
}
