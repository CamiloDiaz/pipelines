package com.herokuapp.cargueroscolombia.question.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.registro.registro.validacionRegistro;


public class registroValidacionQuestion implements Question {
    private String validationCreateAccount;

    public registroValidacionQuestion validacionRegistro(String validation) {
        this.validationCreateAccount = validation;
        return this;

    }
        public static registroValidacionQuestion registroQuestion(){
            return new registroValidacionQuestion();
        }

    @Override
    public Boolean answeredBy(Actor actor) {
        return validacionRegistro.resolveFor(actor).containsOnlyText(validationCreateAccount);
    }
}
