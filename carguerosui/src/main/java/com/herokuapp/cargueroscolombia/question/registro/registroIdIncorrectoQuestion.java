package com.herokuapp.cargueroscolombia.question.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.registro.registro.validacionRegistroIncorrecto;


public class registroIdIncorrectoQuestion implements Question {
    private String registroIdIncorrecto;

    public registroIdIncorrectoQuestion validacionRegistro(String validation) {
        this.registroIdIncorrecto = validation;
        return this;

    }
        public static registroIdIncorrectoQuestion registroIdIncorrectoQuestion(){
            return new registroIdIncorrectoQuestion();
        }

    @Override
    public Boolean answeredBy(Actor actor) {
        return validacionRegistroIncorrecto.resolveFor(actor).containsOnlyText(registroIdIncorrecto);
    }
}
