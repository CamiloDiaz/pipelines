package com.herokuapp.cargueroscolombia.question.iniciarsesion;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.iniciarsesion.iniciarSesion.mensajeDeErrorPorCorreo;

public class IniciarSesionCorreoIncorrecto implements Question <Boolean> {
    private String mensajeCorreoError;

    public IniciarSesionCorreoIncorrecto wasErrorLogInUser(String validation) {
        this.mensajeCorreoError = validation;
        return this;

    }

    public static IniciarSesionCorreoIncorrecto correoIncorrecto(){
        return new IniciarSesionCorreoIncorrecto();
    }


    @Override
    public Boolean answeredBy(Actor actor) {
        return mensajeDeErrorPorCorreo.resolveFor(actor).containsOnlyText(mensajeCorreoError);
    }
}
