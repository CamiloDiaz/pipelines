package com.herokuapp.cargueroscolombia.question.iniciarsesion;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.herokuapp.cargueroscolombia.userinterface.perfildeusuario.PerfilDeUsuarioPagina.nombreCompleto;

public class IniciarSesionExitosamente implements Question <Boolean> {
    private String nombre;
    private String id;
    private String email;
    private String edad;
    private String contacto;
    private String placa;
    private String marca;
    private String anio;
    private String capacidad;
    private String tipoVehiculo;


    public IniciarSesionExitosamente fueLlenadoConElNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConElID(String id) {
        this.id = id;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConElEmail(String email) {
        this.email = email;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConLaEdad(String edad) {
        this.edad = edad;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConElContacto(String contacto) {
        this.contacto = contacto;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConLaPlaca(String placa) {
        this.placa = placa;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConLaMarca(String marca) {
        this.marca = marca;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConElAnio(String anio) {
        this.anio = anio;
        return this;
    }
    public IniciarSesionExitosamente fueLlenadoConLaCapacidad(String capacidad) {
        this.capacidad = capacidad;
        return this;
    }
    public IniciarSesionExitosamente yFueLlenadoConElTipoDeVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
        return this;
    }
    public IniciarSesionExitosamente es(){
        return this;
    }

    public static IniciarSesionExitosamente iniciarSesionExitosamente(){
        return new IniciarSesionExitosamente();
    }


    @Override
    public Boolean answeredBy(Actor actor) {
        return (
                nombreCompleto
                        .resolveFor(actor)
                        .containsValue(nombre)
        );
    }
}
