package com.herokuapp.cargueroscolombia.model.registro;

public class DatosRegistroModel {


    private String nombreCompleto;
    private String id;
    private String idIncorrecto;
    private String edad;
    private String edadMenor;
    private String edadMayor;
    private String celular;
    private String correo;
    private String correoIncorrecto;
    private String clave;
    private String claveSinMayuscula;
    private String claveCantidadMenorCaracteres;
    private String placa;
    private String placaIncorrecta;
    private String numeroPlaca;
    private String numeroPlacaIncorrecto;
    private String marca;
    private String modelo;
    private String capacidad;
    private String tipoVehiculo;

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdIncorrecto() {
        return idIncorrecto;
    }

    public void setIdIncorrecto(String idIncorrecto) {
        this.idIncorrecto = idIncorrecto;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getEdadMenor() {
        return edadMenor;
    }

    public void setEdadMenor(String edadMenor) {
        this.edadMenor = edadMenor;
    }

    public String getEdadMayor() {
        return edadMayor;
    }

    public void setEdadMayor(String edadMayor) {
        this.edadMayor = edadMayor;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoIncorrecto() {
        return correoIncorrecto;
    }

    public void setCorreoIncorrecto(String correoIncorrecto) {
        this.correoIncorrecto = correoIncorrecto;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClaveSinMayuscula() {
        return claveSinMayuscula;
    }

    public void setClaveSinMayuscula(String claveSinMayuscula) {
        this.claveSinMayuscula = claveSinMayuscula;
    }

    public String getClaveCantidadMenorCaracteres() {
        return claveCantidadMenorCaracteres;
    }

    public void setClaveCantidadMenorCaracteres(String claveCantidadMenorCaracteres) {
        this.claveCantidadMenorCaracteres = claveCantidadMenorCaracteres;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(String numeroPlaca) {
        this.numeroPlaca = numeroPlaca;
    }

    public String getPlacaIncorrecta() {
        return placaIncorrecta;
    }

    public void setPlacaIncorrecta(String placaIncorrecta) {
        this.placaIncorrecta = placaIncorrecta;
    }

    public String getNumeroPlacaIncorrecto() {
        return numeroPlacaIncorrecto;
    }

    public void setNumeroPlacaIncorrecto(String numeroPlacaIncorrecto) {
        this.numeroPlacaIncorrecto = numeroPlacaIncorrecto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }
}
