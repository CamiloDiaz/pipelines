package com.herokuapp.cargueroscolombia.model.service;

public class ServiceModel {

    private String vehicleSelection;

    public String getVehicleSelection() {
        return vehicleSelection;
    }

    public void setVehicleSelection(String vehicleSelection) {
        this.vehicleSelection = vehicleSelection;
    }
}
