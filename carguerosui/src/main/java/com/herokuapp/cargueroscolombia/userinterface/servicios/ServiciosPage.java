package com.herokuapp.cargueroscolombia.userinterface.servicios;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ServiciosPage extends PageObject {

    public static final Target SERVICE_BTN = Target
            .the("Service Button")
            .located(By.id("LinkButton"));

    public static final Target SELECT_VEHICLE = Target
            .the("Select Vehicle")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[2]/div"));

    public static final Target VEHICLE_DATA_VALIDATION = Target
            .the("Vehicle Data")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/pre"));

}
