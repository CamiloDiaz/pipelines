package com.herokuapp.cargueroscolombia.userinterface.registro;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class registro extends PageObject {

    //Registro datos conductor

    public static final Target linkRegistro = Target
            .the("ingresarLinkRegistro")
            .located(By.xpath("//*[@id=\"HiperText\"]"));

    public static final Target nombreApellido = Target
            .the("ingresarNombreCompleto")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[1]/input"));

    public static final Target idConductor = Target
            .the("ingresarId")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[2]/input"));

    public static final Target edad = Target
            .the("ingresarEdad")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[3]/input"));

    public static final Target celular= Target
            .the("ingresarCelular")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[4]/input"));

    public static final Target correoElectronico = Target
            .the("ingresarCorreo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/input"));

    public static final Target clave = Target
            .the("ingresarClave")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/input"));

    public static final Target confirmarClave = Target
            .the("ingresarMismaClave")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/input"));

    //Registro datos vehiculo

    public static final Target placa = Target
            .the("ingresarPlacaVehiculo")
            .located(By.id("placal"));

    public static final Target numeroPlaca = Target
            .the("ingresarNumeroPlacaVehiculo")
            .located(By.id("placan"));


    public static final Target marca = Target
            .the("ingresarMarca")
            .located(By.id("marca"));

    public static final Target modelo = Target
            .the("ingresarModelo")
            .located(By.id("anio"));

    public static final Target capacidad= Target
            .the("ingresarCapacidad")
            .located(By.id("capacidad"));

    public static final Target tipoVehiculo= Target
            .the("ingresarTipoVehiculo")
            .located(By.xpath("//*[@id=\"tipo\"]/input"));

    public static final Target Van= Target
            .the("escogerVan")
            .located(By.xpath("//*[@id=\"tipo\"]/div[2]/div[3]"));

    public static final Target PickUp= Target
            .the("escogerPickUp")
            .located(By.xpath("//*[@id=\"tipo\"]/div[2]/div[1]/span"));


    public static final Target botonRegistro= Target
            .the("botonRegistrarse")
            .located(By.xpath("/html/body/div/div/div/div[3]/button"));


    //Validacion

    public static final Target validacionRegistro= Target
            .the("validacionRegistro")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/span"));

    public static final Target validacionError= Target
            .the("mensajeDeError")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/span"));


    public static final Target validacionRegistroIncorrecto = Target
            .the("validacionRegistroNombreIncorrecto")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/span"));


}
