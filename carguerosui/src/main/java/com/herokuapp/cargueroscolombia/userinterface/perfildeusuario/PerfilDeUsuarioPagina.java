package com.herokuapp.cargueroscolombia.userinterface.perfildeusuario;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PerfilDeUsuarioPagina extends PageObject {

    public static final Target TITULO_REGISTRO = Target
            .the("Titulo Registro")
            .located(By.xpath("//*[@id=\"Text\"]"));

    public static final Target nombreCompleto = Target
            .the("Nombre Completo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[1]/input"));

    public static final Target id = Target
            .the("ID")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[2]/input"));

    public static final Target email = Target
            .the("Email")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[3]/input"));

    public static final Target edad = Target
            .the("Edad")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[4]/input"));

    public static final Target contacto = Target
            .the("Contacto")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[5]/input"));

    public static final Target placa = Target
            .the("Placa")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/input"));

    public static final Target marca = Target
            .the("Marca")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/input"));

    public static final Target anio = Target
            .the("Año")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/input"));

    public static final Target capacidad = Target
            .the("Capacidad")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[4]/input"));

    public static final Target tipo = Target
            .the("Tipo vehículo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[5]/input"));

    public static final Target cerrarSesion = Target
            .the("Botón cerrar sesion")
            .located(By.xpath("/html/body/div/div/div/div[3]/button"));
}
