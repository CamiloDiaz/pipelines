package com.herokuapp.cargueroscolombia.userinterface.iniciarsesion;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import net.serenitybdd.screenplay.targets.Target;

public class iniciarSesion extends PageObject {

    public static final Target ingresarCorreo = Target
            .the("Correo")
            .located(By.id("email"));

    public static final Target ingresarClave = Target
            .the("Contraseña")
            .located(By.id("password"));

    public static final Target botonIniciarSesion = Target
            .the("botonIniciarSesion")
            .located(By.xpath("/html/body/div/div/div/div[1]/div[4]/button"));

    public static final Target mensajeDeError= Target
            .the("mensajeDeError")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[3]/span"));


    public static final Target mensajeDeErrorPorCorreo = Target
            .the("mensajeDeErrorPorCorreo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/span"));


}
