package com.herokuapp.cargueroscolombia.util;

public class Dictionary {

    private Dictionary() {
        throw new AssertionError("Instantiating utility class.");
    }

    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";

    public static final String CORREO_INICIAR_SESION = "micorreo.sofka@hotmail.com";
    public static final String CORREO_INCORRECTO = "prueba.sofka@estonosirve.yaper";
    public static final String CLAVE_INICIAR_SESION = "123456M";
    public static final String CLAVE_INCORRECTA = "11jdusj ";

    public static final String NOT_ADDED_VACANCY_DONE =
            "VACANCY NOT ADDED CHECKED CORRECTLY"                               ;
    public static final String NOT_ADDED_VACANCY_VALIDATION_ERROR
            = "THERE WAS AN ERROR TRYING TO VERIFY THAT NO VACANCY WAS ADDED"   ;

    public static final String OPEN_LANDING_PAGE_DONE =
            "LANDING PAGE SUCCESSFULLY OPENED";

    public static final String OPEN_LANDING_PAGE_VALIDATION_ERROR =
            "THERE WAS AN ERROR TRYING TO OPEN LANDING PAGE";

    public static final String SELECT_VEHICLE_DONE =
            "VEHICLE CORRECTLY SELECTED";

    public static final String SELECT_VEHICLE_VALIDATION_ERROR =
            "THERE WAS AN ERROR TRYING TO SELECT A VEHICLE";

    public static final String DRIVER_INFO_DONE =
            "DRIVER INFORMATION CORRECTLY SHOWED";

    public static final String DRIVER_INFO_VALIDATION_ERROR =
            "THERE WAS AN ERROR TRYING TO SHOW DRIVER INFORMATION";
}
