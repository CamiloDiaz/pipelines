package com.herokuapp.cargueroscolombia.util;

import com.github.javafaker.Faker;
import com.herokuapp.cargueroscolombia.model.service.ServiceModel;

import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.ONE;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.THREE;

public class ServiceModelData {

    private ServiceModelData() {
        throw new AssertionError("Instantiating utility class.");
    }

    public static ServiceModel serviceModelInformation(){

        Faker faker = new Faker();

        String serviceVehiclePosition = String.valueOf(faker.number().numberBetween(ONE.getValue(), THREE.getValue()));

        ServiceModel serviceModel = new ServiceModel();

        serviceModel.setVehicleSelection(serviceVehiclePosition);

        return serviceModel;
    }

}
