package com.herokuapp.cargueroscolombia.util;


import com.github.javafaker.Faker;
import com.herokuapp.cargueroscolombia.model.registro.DatosRegistroModel;

import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.*;

public class DatosRegistro {

    private static DatosRegistroModel random;
    private static Faker DatosRandom;

    private static String primerNombre;
    private static String apellido;
    private static String nombreCompleto;

    private static String id;
    private static String idIncorrecto;

    private static String edad;
    private static String edadMenor;
    private static String edadMayor;

    private static String celular;
    private static String correo;

    private static String correoIncorrecto;
    private static String clave;
    private static String claveSinMayuscula;
    private static String claveCantidadMenorCaracteres;
    private static String placa;
    private static String placaIncorrecta;
    private static String numeroPlaca;
    private static String numeroPlacaIncorrecta;
    private static String marca;
    private static String modelo;
    private static String modeloMenor;
    private static String capacidad;
    private static String tipoVehiculo;

    private static  int tipoVehiculos;
    private static int marcaVehiculo;



    public static DatosRegistroModel generarPersonasRandom(){
        //Datos Random
        DatosRandom = new Faker();

        primerNombre = DatosRandom.name().firstName();
        apellido = DatosRandom.name().lastName();
        nombreCompleto = primerNombre +" "+ apellido;


        int numId = DatosRandom.number().numberBetween(100000000, 199999999);
        id = String.valueOf(numId) + FOUR.getValue();

        int numIdInc = DatosRandom.number().numberBetween(100000, 199999);
        idIncorrecto = String.valueOf(numIdInc) + FOUR.getValue();

        edad = String.valueOf(DatosRandom.number().numberBetween(18,66));
        int numIde = DatosRandom.number().numberBetween(100000000, 199999999);
        celular = String.valueOf(numIde) + FOUR.getValue();
        correo = DatosRandom.internet().emailAddress();
        edadMenor=String.valueOf(DatosRandom.number().numberBetween(1,17));
        edadMayor=String.valueOf(DatosRandom.number().numberBetween(67,95));


        correo =DatosRandom.internet().emailAddress() ;
        correoIncorrecto = "awse.com";
        clave = "Asd123456";
        claveSinMayuscula="asd123456";
        claveCantidadMenorCaracteres="Asd12";

        placa =  DatosRandom.letterify("???").toUpperCase();

        modelo =String.valueOf(DatosRandom.number().numberBetween(2000,2022));
        numeroPlaca = String.valueOf(DatosRandom.number().numberBetween(111,999));

        capacidad = String.valueOf(DatosRandom.number().numberBetween(10,100));


        tipoVehiculos = DatosRandom.number().numberBetween(ZERO.getValue(), THREE.getValue());
        marcaVehiculo = DatosRandom.number().numberBetween(ZERO.getValue(), SIX.getValue());

        tipoVehiculo = escogerTipo(tipoVehiculos);
        marca = escogerMarca(marcaVehiculo);
        placaIncorrecta = "A";
        numeroPlacaIncorrecta = "2";
        modeloMenor = String.valueOf(NINETEEN_SEVENTY.getValue());



        random = new DatosRegistroModel();
        random.setNombreCompleto(nombreCompleto);
        random.setId(id);
        random.setIdIncorrecto(idIncorrecto);
        random.setEdad(edad);
        random.setEdadMenor(edadMenor);
        random.setEdadMayor(edadMayor);
        random.setCelular(celular);
        random.setCorreo(correo);
        random.setCorreoIncorrecto(correoIncorrecto);
        random.setClave(clave);
        random.setClaveSinMayuscula(claveSinMayuscula);
        random.setClaveCantidadMenorCaracteres(claveCantidadMenorCaracteres);
        random.setPlaca(placa);
        random.setPlacaIncorrecta(placaIncorrecta);
        random.setNumeroPlaca(numeroPlaca);
        random.setNumeroPlacaIncorrecto(numeroPlacaIncorrecta);
        random.setMarca(marca);
        random.setModelo(modelo);
        random.setCapacidad(capacidad);
        random.setTipoVehiculo(tipoVehiculo);

        return random;
    }

    public static String escogerTipo(int tipoVehiculo){
        String[] tiposVehiculos = {"Van", "Camión", "Pick up"};
        return tiposVehiculos[tipoVehiculo];
    }

    public static String escogerMarca(int marcaVehiculo){
        String[] marcaVehiculos = {"Chevrolet", "Renault", "Mazda", "Toyota", "Hiundai", "Kia"};
        return marcaVehiculos[marcaVehiculo];
    }


}
