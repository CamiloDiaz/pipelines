package com.herokuapp.cargueroscolombia.task.servicios;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.herokuapp.cargueroscolombia.userinterface.servicios.ServiciosPage.SERVICE_BTN;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class BrowseToServicePage implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(SERVICE_BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to(SERVICE_BTN),
                Click.on(SERVICE_BTN)
        );

    }

    public static BrowseToServicePage browseToServicePage(){
        return new BrowseToServicePage();
    }
}
