package com.herokuapp.cargueroscolombia.task.registro;


import net.serenitybdd.screenplay.Actor;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;

import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.herokuapp.cargueroscolombia.userinterface.registro.registro.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class registro implements Task {


    private String nombreApellidoRegistro;
    private String idRegistro;
    private String edadRegistro;
    private String celularRegistro;
    private String correoRegistro;
    private String claveRegistro;
    private String confirmarClaveRegistro;


    private String placaRegistro;
    private String NumeroplacaRegistro;
    private String marcaRegistro;
    private String modeloRegistro;
    private String capacidadRegistro;
    private String tipoVehiculos;

    //Registro datos conductor

    public registro nombreApellidoRegistro(String Nombre) {
        this.nombreApellidoRegistro = Nombre;
        return this;
    }

    public registro idRegistro(String id) {
        this.idRegistro = id;
        return this;
    }

    public registro edadRegistro(String Edad) {
        this.edadRegistro = Edad;
        return this;
    }

    public registro celularRegistro(String Celular) {
        this.celularRegistro = Celular;
        return this;
    }

    public registro correoRegistro(String Correo) {
        this.correoRegistro = Correo;
        return this;
    }

    public registro claveRegistro(String clave) {
        this.claveRegistro = clave;
        return this;
    }

    public registro confirmarClaveRegistro(String confirmarClave) {
        this.confirmarClaveRegistro = confirmarClave;
        return this;
    }


    //Registro datos vehiculo

    public registro placaRegistro(String Placa) {
        this.placaRegistro = Placa;
        return this;
    }

    public registro numeroPlacaRegistro(String numPlaca) {
        this.NumeroplacaRegistro = numPlaca;
        return this;
    }

    public registro marcaRegistro(String Marca) {
        this.marcaRegistro = Marca;
        return this;
    }

    public registro modeloRegistro(String Modelo) {
        this.modeloRegistro = Modelo;
        return this;
    }

    public registro capacidadRegistro(String Capacidad) {
        this.capacidadRegistro = Capacidad;
        return this;
    }

    public registro tipoVehiculos(String tipo) {
        this.tipoVehiculos = tipo;
        return this;
    }





    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                //Acciones del registros datos conductor

                Scroll.to(nombreApellido),
                Enter.theValue(nombreApellidoRegistro).into(nombreApellido),

                Scroll.to(idConductor),
                Enter.theValue(idRegistro).into(idConductor),

                Scroll.to(edad),
                Enter.theValue(edadRegistro).into(edad),

                Scroll.to(celular),
                Enter.theValue(celularRegistro).into(celular),

                Scroll.to(correoElectronico),
                Enter.theValue(correoRegistro).into(correoElectronico),

                Scroll.to(clave),
                Enter.theValue(claveRegistro).into(clave),

                Scroll.to(confirmarClave),
                Enter.theValue(confirmarClaveRegistro).into(confirmarClave),

                //Acciones del registros datos vehiculo

                Scroll.to(placa),
                Enter.theValue(placaRegistro).into(placa),

                Scroll.to(numeroPlaca),
                Enter.theValue(NumeroplacaRegistro).into(numeroPlaca),

                Scroll.to(marca),
                Enter.theValue(marcaRegistro).into(marca),

                Scroll.to(modelo),
                Enter.theValue(modeloRegistro).into(modelo),

                Scroll.to(capacidad),
                Enter.theValue(capacidadRegistro).into(capacidad),

                WaitUntil.the(tipoVehiculo, isVisible()),

                Scroll.to(tipoVehiculo),
                SendKeys.of(tipoVehiculos).into(tipoVehiculo),

                WaitUntil.the(botonRegistro, isVisible()),
                Scroll.to(botonRegistro),
                Click.on(botonRegistro)


                );
        
    }

    public static registro registroDatos(){
        return new registro();
    }
}
