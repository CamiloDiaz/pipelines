package com.herokuapp.cargueroscolombia.task.iniciarsesion;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.herokuapp.cargueroscolombia.userinterface.iniciarsesion.iniciarSesion.*;
import static com.herokuapp.cargueroscolombia.userinterface.perfildeusuario.PerfilDeUsuarioPagina.cerrarSesion;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class iniciarSesion implements Task {

    private String correoInicioSesion;
    private String claveInicioSesion;

    public iniciarSesion correoIniciarSesion(String Correo) {
        this.correoInicioSesion = Correo;
        return this;
    }

    public iniciarSesion claveIniciarSesion(String clave) {
        this.claveInicioSesion = clave;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(ingresarCorreo, isVisible()).forNoMoreThan(10).seconds(),
                Scroll.to(ingresarCorreo),
                Enter.theValue(correoInicioSesion).into(ingresarCorreo),

                Scroll.to(ingresarClave),
                Enter.theValue(claveInicioSesion).into(ingresarClave),

                Scroll.to(botonIniciarSesion),
                Click.on(botonIniciarSesion)
        );
        
    }

    public static iniciarSesion sesion(){
        return new iniciarSesion();
    }
}
