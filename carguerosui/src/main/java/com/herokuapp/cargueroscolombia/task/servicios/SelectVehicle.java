package com.herokuapp.cargueroscolombia.task.servicios;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.herokuapp.cargueroscolombia.userinterface.servicios.ServiciosPage.SELECT_VEHICLE;
import static com.herokuapp.cargueroscolombia.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectVehicle implements Task {

    private String serviceVehicle;

    public SelectVehicle choosingAnOptionOfVehicle(String serviceVehicle) {
        this.serviceVehicle = serviceVehicle;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the   (SELECT_VEHICLE,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to(SELECT_VEHICLE),
                SelectFromOptions.byValue(serviceVehicle).from(SELECT_VEHICLE)
        );

    }

    public static SelectVehicle selectVehicle(){
        return new SelectVehicle();
    }
}
