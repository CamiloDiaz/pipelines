package com.herokuapp.cargueroscolombia.task.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;


import static com.herokuapp.cargueroscolombia.userinterface.registro.registro.linkRegistro;

public class BrowserToRegistro implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Scroll.to(linkRegistro),
                Click.on(linkRegistro)

                        );
    }

    public static BrowserToRegistro toRegistro(){
        return new BrowserToRegistro();
    }
}
